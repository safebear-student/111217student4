package com.bdd.app;

import com.bdd.app.pages.ConfigurePage;
import com.bdd.app.pages.LoginPage;
import com.bdd.app.pages.ReportingPage;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 11/12/2017.
 */

public class StepDefs {

    String url;
    WebDriver driver;
    LoginPage loginPage;
    ReportingPage reportingPage;
    ConfigurePage configurePage;

    @Before
    public void setUp(){
        driver = new ChromeDriver();
        url = "http://simplerisk.local/index.php";
        loginPage = new LoginPage(driver);
        configurePage = new ConfigurePage(driver);
        reportingPage = new ReportingPage(driver);

        driver.get(url);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void tearDown(){
       try {
           reportingPage.logout();
       }catch(Exception e){
       }
       driver.quit();
    }

    @Given("^a user called (.+) with (.+) permissions and password (.+)$")
    public void a_user_called_simon_with_adminstrator_permissions(String username, String permission, String password) throws Throwable {
        loginPage.enterUsername("admin");
        loginPage.enterPassword("^Y&U8i9o0p");
        loginPage.login();
        reportingPage.clickConfigureLink();
        configurePage.clickUserManagementLink();
        if(configurePage.checkIfUserIsPresent(username)){
            configurePage.logout();
        }else{
            configurePage.enterNewUsername(username);
            configurePage.enterNewPassword(password);
            configurePage.givePermissions(permission);
            configurePage.createUser();
            configurePage.logout();
        }
    }

    @When("^(.+) is logged in with password (.+)$")
    public void administrator_is_logged_in(String username, String password) throws Throwable {
        loginPage.enterUsername(username);
        loginPage.enterPassword(password);
        loginPage.login();
    }

    @Then("^he is able to view (.+)'s account$")
    public void able_to_view_accounts(String username) throws Throwable {
       reportingPage.clickConfigureLink();
       configurePage.clickUserManagementLink();
       configurePage.viewUserDetails(username);
       assertTrue(configurePage.checkUserDetails(username));
    }
}
