package com.bdd.app;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by CCA_Student on 11/12/2017.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        tags = "~@to-do",
        glue = "com.bdd.app",
        features = "classpath:simplerisk.features/user_management.feature",
        plugin ={"pretty", "html:target/cucumber"}
)

public class RunCukesTest {

}
