Feature: User Management

  User Story: (optional)
  In order to manage users
  As an administrator
  I need CRUD permission to user accounts

  Rules:
  -Non admin users have no permissios over other accounts
  -There are two types on non admin users
    - Risk managers
    - Asset managers

  Questions:
  To do:
  Domain Language


  Background:
    Given a user called simon with administrator permissions and password S@feB3er
    And a user called tom with risk_management permissions and password S@feB3er

  @high-impact
  Scenario Outline:The administrator checks a user's details
    When simon is logged in with password S@feB3er
    Then he is able to view <user>'s account
    Examples:
      | user |
      | tom  |

  @high-risk
  @to-do
  Scenario Outline:A user's password is reset by the administrator
   Given a <user> has lost his password
    When simon is logged in with password S@feB3ar
    Then simon can reset <user>'s password
    Examples:
      | user |
      | tom  |
